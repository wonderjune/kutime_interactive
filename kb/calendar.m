#(domain)
ID calendar
DESC 달력
KMATCH N
KWORDS /BEGIN LIST
달력
일정
/END LIST
USING /BEGIN LIST
accdate
dayweek
daterel
numericpre
numeric
acctime
langvx
langwhen
langwhat
langquoten
langquotev
readv
writev
writen
readorwritev
/END LIST
VARS /BEGIN LIST
Action    readv writev writen readorwritev
Content   langwhat langquoten langquotev
DayOfWeek dayweek
Date      accdate daterel
Time      acctime langwhen
/END LIST
VNASSN /BEGIN LIST
Action    read
/END LIST
RESBASE /FILE calendar.mdr
RESSCHEMA /BEGIN LIST
Date
DayOfWeek
Time
Content
/END LIST
REACT /BEGIN LIST
{?Action read {!Date {=Date} }{!DayOfWeek {=DayOfWeek} }{!Time {=Time} }일정입니다.}{?Action write {!Date {=Date} }{!DayOfWeek {=DayOfWeek} }{!Time {=Time} }에 일정을 씁니다.}
/END LIST
