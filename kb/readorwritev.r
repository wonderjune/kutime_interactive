#(category)
ID readorwritev
DESC 읽기 또는 쓰기 (용언어간)
MATCH V
DNINFER message
WORDS /BEGIN LIST
알
알려
알려주
/END LIST
REWRITE /BEGIN LIST
알려주 read|write
알려 read|write
알 read|write
/END LIST
