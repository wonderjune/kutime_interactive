#(category)
ID writev
DESC 쓰기 용언어간
MATCH V
DNINFER message
WORDS /BEGIN LIST
주
드리
보내
보내주
만들
만들어주
쓰
써
써주
올리
올려
올려주
넣
넣어
넣어주
만들
만들어주
/END LIST
REWRITE /BEGIN LIST
주 write
드리 write
보내주 write
보내 write
써주 write
써 write
쓰 write
올려주 write
올려 write
올리 write
넣어주 write
넣어 write
넣 write
만들어주 write
만들 write
/END LIST
