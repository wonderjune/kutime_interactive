#(category)
ID readv
DESC 읽기 (용언어간)
MATCH V
DNINFER gensearch
WORDS /BEGIN LIST
읽
읽어주
띄우
띄워
띄워주
찾
찾아
찾아주
보이
보여
보여주
/END LIST
REWRITE /BEGIN LIST
읽어주 read
읽 read
띄워주 read
띄워 read
띄우 read
찾아주 read
찾아 read
찾 read
보여주 read
보여 read
보이 read
/END LIST
