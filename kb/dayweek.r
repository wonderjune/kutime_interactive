#(category)
ID dayweek
DESC 요일
MATCH N+
WORDS /BEGIN LIST
월요일
화요일
수요일
목요일
금요일
토요일
일요일
/END LIST
REWRITE /BEGIN LIST
월요일 Mon
화요일 Tue
수요일 Wed
목요일 Thu
금요일 Fri
토요일 Sat
일요일 Sun
/END LIST
