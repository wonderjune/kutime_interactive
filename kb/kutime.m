#(domain)
ID kutime
DESC 고려대 수강편람
KMATCH N
KWORDS /BEGIN LIST
강의
수업
과목
/END LIST
USING /BEGIN LIST
kutimeobj
_n
_z
desig
lecturer
lecturerdesig
course
dayweek
numericpre
numeric
numericdigit
acctime
langvx
langwhose
langwhere
langwhen
langwhat
readv
readorwritev
/END LIST
VARS /BEGIN LIST
Action    readv readorwritev writev writen
Lecturer  lecturer desig lecturerdesig langwhose
Place     langwhere
Section   langwhat
DayOfWeek dayweek
Time      acctime
/END LIST
VNASSN /BEGIN LIST
Action    read
/END LIST
RESBASE /FILE kutime.mdr
RESSCHEMA /BEGIN LIST
Section
ClassType
Place
Lecturer
Description
DayOfWeek Time
/END LIST
REACT /BEGIN LIST
{?Action read {!Lecturer {=Lecturer} 교수님의 }{!DayOfWeek {=DayOfWeek} }{!Section {=Section} }{!Place {=Place}에서 열리는 }수업 정보입니다.}{?Action write 강의편람에 쓸 수 없습니다. 정보를 찾은 후에 달력에 쓰세요.}
/END LIST
