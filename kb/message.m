#(domain)
ID message
DESC 문자보내기
KMATCH N
KWORDS /BEGIN LIST
문자
문자메시지
/END LIST
USING /BEGIN LIST
friend
frienddesig
lecturer
lecturerdesig
dayweek
daterel
numericpre
numeric
acctime
langvx
langwhom
langwhose
langwhen
langwhat
langquoten
langquotev
readv
writev
writen
readorwritev
/END LIST
VARS /BEGIN LIST
Action    writev writen readorwritev readv
Whom      langwhom
Whose     langwhose
When      dayweek daterel acctime langwhen
Content   langwhat langquoten langquotev
/END LIST
VNASSN /BEGIN LIST
Action    write
/END LIST
RESSCHEMA /BEGIN LIST
Whom
Whose
When
Content
/END LIST
REACT /BEGIN LIST
{?Action read {!Whom {=Whom}(으)로부터 }{!Whose {=Whose}(으)로부터 }{!When {=When}에 받은 }메시지 정보입니다.}{?Action write {!Whom {=Whom}에게 }{!Whose {=Whose}에게 }{!When {=When}에 보내는 }메시지 정보입니다.}
/END LIST
