#(domain)
# This domain is passive.
ID gensearch
DESC 웹 검색
KMATCH N
KWORDS /BEGIN LIST
검색
/END LIST
USING /BEGIN LIST
langwhat
langquoten
langquotev
/END LIST
VARS /BEGIN LIST
Keyword   langwhat langquoten langquotev langwhere
Action
/END LIST
VNASSN /BEGIN LIST
Action    read
/END LIST
RESSCHEMA /BEGIN LIST
Keyword
Result
/END LIST
REACT /BEGIN LIST
{?Action read {?Keyword {=Keyword}을(를)} 인터넷에서 검색합니다.}{?Action write 검색을 하고 싶으십니까?}
/END LIST
