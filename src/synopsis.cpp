#include "synopsis.h"
#include "utils.h"
#include "debug.h"
#include <iostream>
#include <cstdio>
#include <cstring>
#include <set>
#include <vector>
#include <string>

void take_action(std::map<std::string, kuti_module> &modules, kuti_synopsis &synopsis);
static void proc_local_error(kuti_error e);
std::string subst(kuti_synopsis &, const std::string &);

void proc_synopsis(kuti_result &result, kuti_synopsis &synopsis)
{
	try {
		std::string domain = "";
		for (const auto &it : result.packed) {
			for (const auto &jt : it.category) {
				if (kuti_module::all().find(jt) != kuti_module::all().end()) {
					if (domain.empty())
						domain = jt;
					else
						throw eCannotReduce;
				}
			}
		}

		if (domain.empty()) {
			for (const auto &it : result.packed) {
				for (const auto &jt : it.category) {
					const auto kt = kuti_rule::all().find(jt);
					if (kt != kuti_rule::all().end() && !(*kt).second.dninfer.empty()) {
						if ((*kt).second.dninfer.size() > 1)
							throw eCannotReduce;
						if (domain.empty())
							domain = (*kt).second.dninfer[0];
					}
				}
			}
		}

		if (domain.empty())
			throw eCannotReduce;

		const kuti_module &mod = kuti_module::all()[domain];

		synopsis.module = mod.id;
		for (auto it = mod.vars.begin(); it != mod.vars.end(); it++) {
			std::vector<std::string> candidate_keys;
			std::string rest = (*it).second;
			do {
				std::string first;
				split(PARSE_DELIM, rest, first, rest);
				if (first.empty())
					break;
				else
					candidate_keys.push_back(first);
			} while (!rest.empty());

			bool found = false;
			bool blank;
			std::string value;
			for (auto it = result.packed.begin(); !found && it != result.packed.end(); it++) {
				blank = false;
				for (const auto &jt : it->category) {
					if (jt == "_") blank = true;
					for (const auto &kt : candidate_keys) {
						if (found) break;
						if (jt == kt) {
							found = true;
							value = it->stem;
						}
					}
				}
			}
			
			if (found) {
				if (blank)
					synopsis.vars.insert(std::make_pair((*it).first, "_"));
				else
					synopsis.vars.insert(std::make_pair((*it).first, value));
			}
		}

		take_action(kuti_module::all(), synopsis);
	} catch (kuti_error_label e) {
		proc_local_error(e);
	} catch (kuti_error e) {
		proc_local_error(e);
	}
}

/*
 REACT text preprocessing format
 {=name} vars[name]
 {!name rest} if vars[name] not null, rest
 {?name value rest} if vars[name] eq value, rest
 */
void take_action(std::map<std::string, kuti_module> &modules, kuti_synopsis &synopsis)
{
	/** variable assumption for null/ambiguous */
	for (const auto &a : modules[synopsis.module].vnassn) {
		if (synopsis.vars.find(a.first) == synopsis.vars.end() ||
			synopsis.vars[a.first].find(a.second) != synopsis.vars[a.first].npos && synopsis.vars[a.first] != a.second)
			synopsis.vars[a.first] = a.second;
	}

#if LOG & DEBUG
	printf(DBG_FMT_DELIM, "variables assigned");
	printf("<%s>\n", synopsis.module.c_str());
	for (const auto &var : synopsis.vars) {
		printf("%s: %s\n", var.first.c_str(), var.second.c_str());
	}
#endif

	std::string react = modules[synopsis.module].react;
	std::string msg = subst(synopsis, react);
	if (!msg.empty()) {
		printf(DBG_FMT_DELIM, "answer");
		printf("%s\n", msg.c_str());
	} else {
		printf(DBG_FMT_DELIM, "answerless");
	}
	
	auto &rb = kuti_database::all()[modules[synopsis.module].resbase];
	kuti_database filtrd(rb.cols, rb.column);

	if (synopsis.vars["Action"] == "read") {
		if (modules[synopsis.module].resbase == -1) return;

		std::vector<std::map<std::string, std::string> > values;

		std::map<int, std::vector<std::string> > colmap;
		int rbcols = rb.cols;
		const auto &rbcoln = rb.column;
		for (int i = 0; i < rbcols; i++) {
			auto &s = rbcoln[i];
			std::vector<std::string> l;
			per_split(" ", s, [&l](const std::string tok){ l.push_back(tok); });

			if (!l.empty())
				colmap[i] = l;
		}
		for (auto it = rb.rows.cbegin(); it != rb.rows.cend(); it++) {
			bool match = true;
			for (int i = 0; i < rb.cols && match; i++) {
				std::string s;
				if (colmap.find(i) != colmap.end()) {
					for (const auto &var : synopsis.vars) {
						if (var.second != "_") {
							for (const std::string &rix : colmap[i]) {
								if (var.first == rix)
									s += var.second;
							}
						}
					}
				}
				if (!s.empty() && (*it).data[i].find(s) == (*it).data[i].npos) {
					match = false;
				}
			}
		
			if (match) {
				filtrd.rows.push_back(kuti_database::row(*it));
			}
		}
	
		printf(DBG_FMT_DELIM, "result");
		for (int i = 0; i < filtrd.cols; i++)
			printf("%s\t", filtrd.column[i].c_str());
		puts("");
		if (filtrd.rows.empty()) {
			puts("--------값 없음--------");
		} else {
			for (const auto &r : filtrd.rows) {
				for (int i = 0; i < filtrd.cols; i++) {
					printf("%s\t", r.data[i].c_str());
				}
				puts("");
			}
		}
	} else if (synopsis.vars["Action"] == "write") {
		printf(DBG_FMT_DELIM, "result");
		for (int i = 0; i < filtrd.cols; i++)
			printf("%s\t", filtrd.column[i].c_str());
		puts("");
		
		kuti_database::row row;
		for (int i = 0; i < rb.cols; i++) {
			for (const auto &t : synopsis.vars) {
				if (rb.column[i] == t.first) {
					row.data[i] = t.second;
				}
			}
		}
		rb.rows.insert(rb.rows.end(), row);

		if (synopsis.vars.empty()) {
			puts("--------값 없음--------");
		} else {
			for (int i = 0; i < filtrd.cols; i++) {
				printf("%s\t", synopsis.vars[filtrd.column[i]].c_str());
			}
			puts("");
		}
	} else {
		throw eCannotReduce;
	}
}

static void proc_local_error(kuti_error e)
{
	switch (e.label) {
	case eNoSuchAction:
		puts("다시 말씀해 주세요. 할 수 없는 일일지도 모릅니다.");
		break;

	case eNotYetImplemented:
		puts("뭘 원하시는지는 알겠지만 지능이 모자랍니다...");
		break;

	case eCannotReduce:
		puts("뭘 원하시는지 잘 모르겠군요...");
		break;

	case eDebugOnly:
		puts("문제가 있지만 지능이 모자라서 이유를 모릅니다.");
		break;

	default:
		throw e;
	}
}
