/**	KUTi: KUTIME_INTERACTIVE
 *
 *	@author Ki Heon Kim, Eon Jeong
 *	@url https://bitbucket.org/wonderjune/kutime_interactive
 *
 *	@section description
 *	Fall 2013 CNCE330 Artificial Intelligence term project
 *	A smart time table assistant.
 *	The name is inspired by KUTime; a time table application for Korea Univ. students.
 */

#include "debug.h"
#include "result.h"
#include "synopsis.h"
#include "error.h"

static char ex_path[1024];

static void kb_fopen(std::ifstream &fin, const char *filename);
static int get_line(FILE *fp, unsigned char *sent, char inputmode);
static void proc_morph(FILE *fp, HAM_PMORES out1, HAM_PRUNMODE mode);

int main(int argc, char **argv)
{
	// assume executable is built on (project_root)/build/(Debug|Release|something)
	strcpy(ex_path, argv[0]);
	*strrchr(ex_path, '\\') = '\0';

	// load modules
	do {
		char mod_path[1024];
		std::ifstream fin;
		const char *mod_name[] = {"kutime", "calendar", "post", "message", "gensearch", nullptr};

		for (int i = 0; mod_name[i] != nullptr; i++) {
			sprintf(mod_path, "%s/../../kb/%s.m", ex_path, mod_name[i]);

			fin.open(mod_path, std::ios::in);

			kuti_module::load(mod_name[i], fin, kb_fopen);
			
			fin.close();
		}
	} while (0);
	
	// get input, use klt, run kuti
	do {
		char ini_path[1024];
		int flag;
		HAM_RUNMODE mode;
		unsigned char sent[SENTSIZE];
		HAM_PMORES	hamout1;

		sprintf(ini_path, "%s/../../hdic/KLT2000.ini", ex_path);

		flag = open_HAM(&mode, "i", ini_path);

		if (flag) {
			fprintf(stderr, "KLT ini load error!\n");
			return 1;
		}

		while (get_line(stdin, sent, mode.inputmode)) {
			hamout1 = morph_anal(sent, NULL, &mode);
			try {
				proc_morph(stdout, hamout1, &mode);
			} catch (kuti_error_label e) {
				proc_error(e);
			} catch (kuti_error e) {
				proc_error(e);
			}
			puts("");
		}

		close_HAM();
	} while (0);

	return 0;
}

void kb_fopen(std::ifstream &fin, const char *filename)
{
	char path[1024];
	sprintf(path, "%s/../../kb/%s", ex_path, filename);
	fin.open(path, std::ios::in);
}

int get_line(FILE *fp, unsigned char *sent, char inputmode)
{
	printf("> ");
	if (inputmode || 1) {	/* always line-by-line input */
		if (fgets((char *)sent, SENTSIZE-1, fp))
			return 1;
		else return 0;
	}
}

void proc_morph(FILE *fp, HAM_PMORES out1, HAM_PRUNMODE mode)
{
	kuti_result result;
	kuti_synopsis synopsis;

	if (!out1->nword) {
		return; // empty input failsafe
	}
	
#if LOG & DEBUG
	printf(DBG_FMT_DELIM, "morpheme analysis result");
#endif
	for (int i = 0; i < out1->nword; ++i) {
		HAM_PWORD pw = &out1->word[i];
		HAM_PRESULT pr = &pw->result[pw->gr[1]]; // use the best result only

		kuti_morphw morph;
		morph.phresult = pr;
		result.push_back(morph);

#if LOG & DEBUG
		dbg_put_presult(stdout, pr, mode);
#endif
	}
	
	try {
		proc_result(result);
		proc_synopsis(result, synopsis);
	} catch (kuti_error_label e) {
		if (e != eSuccess) {
			result.clear();
			throw e;
		}
	} catch (kuti_error e) {
		if (e.label != eSuccess) {
			result.clear();
			throw e;
		}
	}

	result.clear();
}
