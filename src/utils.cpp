#include "utils.h"

void split(const char *const delim, const std::string line, std::string &first_out, std::string &rest_out)
{
	const char *s = line.c_str();
	const char *const pdelim = strpbrk(s, delim);

	if (pdelim != nullptr) {
		char first_temp[1024];
		strncpy(first_temp, s, pdelim - s);
		first_temp[pdelim - s] = '\0';
		first_out = std::string(first_temp);
		rest_out = std::string(pdelim + strspn(pdelim, delim));
	} else {
		first_out = line;
		rest_out = std::string();
	}
}

void per_split(const char *const delim, const std::string line, std::function<void (const std::string &)> ftor)
{
	std::string rest = line;
	do {
		std::string first;
		split(delim, rest, first, rest);
		if (first.empty())
			break;
		else
			ftor(first);
	} while (!rest.empty());
}
