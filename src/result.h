#pragma once

#include "common.h"
#include "types.h"

void proc_result(kuti_result &result);

void proc_rule(kuti_result &result, std::map<std::string, kuti_rule> &rule);
