#pragma once
#include <string>
#include <functional>

#define PARSE_DELIM ":;, \r\n\t"
void split(const char *const, const std::string, std::string &, std::string &);
void per_split(const char *const, const std::string, std::function<void (const std::string &)>);
