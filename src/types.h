#pragma once

#include "common.h"
#include <map>
#include <string>
#include <vector>
#include <list>
#include <fstream>

enum kuti_error_label;
struct kuti_error;
struct kuti_module;
struct kuti_rule;
struct kuti_morphw;
struct kuti_result;
struct kuti_database;
struct kuti_synopsis;

typedef void (*kb_fopen_t)(std::ifstream &fin, const char *filename);

enum kuti_error_label {
	eSuccess,
	eBadModule,
	eParseFailed,
	eBadSemantic,
	eVerbNotFound,
	eCannotReduce,
	eNotYetImplemented,
	eNoSuchAction,
	eUndefined,
	eDebugOnly
};

struct kuti_error {
	enum kuti_error_label label;
	int data;
	kuti_error();
	kuti_error(enum kuti_error_label l);
	kuti_error(enum kuti_error_label l, int data);
	kuti_error &operator=(const enum kuti_error_label &l);
	bool operator==(kuti_error &e) const;
	bool operator==(enum kuti_error_label &l) const;
};

struct kuti_module {
	std::string id;
	std::string desc;
	std::vector<std::string> extends;
	std::vector<std::string> deplist;
	std::string kmatch;
	std::vector<std::string> kwords;
	std::vector<std::pair<std::string, std::string>> vars;
	std::map<std::string, std::string> vnassn;
	std::string react;
	int resbase;

	kuti_module() : id(), desc(), extends(), deplist(), kmatch(), kwords(), vars(), vnassn(), react(), resbase(-1) {}
	
	static std::map<std::string, kuti_module> &all();
	static void load(std::string name, std::ifstream &fin, kb_fopen_t fopend);
	
private:
	static std::map<std::string, kuti_module> _all;
};

struct kuti_rule {
	std::string id;
	std::string desc;
	std::vector<std::string> dninfer;
	std::vector<std::string> extends;
	std::string match;
	std::vector<std::string> words;
	std::vector<std::pair<std::string, std::string> > rewrite;
	std::vector<std::pair<std::string, std::string> > replace;

	kuti_rule() : id(), desc(), dninfer(), extends(), match(), words(), rewrite(), replace() {}
	
	static std::map<std::string, kuti_rule> &all();
	static void load(std::string name, std::ifstream &fin, kb_fopen_t fopend);

private:
	static std::map<std::string, kuti_rule> _all;
};

struct kuti_morphw {
	HAM_PRESULT phresult;
	std::vector<std::string> category;

	kuti_morphw() : phresult(nullptr), category() {}
	kuti_morphw(HAM_PRESULT phresult) : phresult(phresult), category() {}
};

struct kuti_wordpack {
	std::string stem;
	char pos;
	std::vector<std::string> category;
	kuti_wordpack(std::string s, char p, std::vector<std::string> c) :
		stem(s), pos(p), category(c) {}
};

struct kuti_result : std::vector<kuti_morphw> {
	std::multimap<std::string, int> distn;
	std::vector<kuti_wordpack> packed;

	kuti_result() : distn(), packed() {}
};

struct kuti_database {
	template <int n>
	struct kuti_database_row;
	typedef kuti_database_row<100> row;

	int cols;
	std::string column[100];
	std::list<row> rows;
	kuti_database() : cols(0), column(), rows() {}
	kuti_database(const int cols, const std::string (&column)[100]) : cols(cols), column(), rows() {
		for (int i = 0; i < 100; i++) this->column[i] = column[i];
	}
	kuti_database(const kuti_database &db) : cols(db.cols), column(), rows(db.rows) {
		for (int i = 0; i < 100; i++) this->column[i] = db.column[i];
	}

	static std::vector<kuti_database> &all();
private:
	static std::vector<kuti_database> _all;

	template <int n>
	struct kuti_database_row {
		std::string data[n];
		
		kuti_database_row () : data() {}
		kuti_database_row (const kuti_database_row<n> &row) : data() {
			for (int i = 0; i < n; i++) data[i] = row.data[i];
		}
	};
};

struct kuti_synopsis {
	std::string module;
	std::map<std::string, std::string> vars;
};
