#include "result.h"
#include "debug.h"
#include "error.h"
#include <set>
#include <iostream>
#include <functional>
using namespace std;

void apply_rule(std::vector<struct kuti_wordpack> &words, std::map<std::string, kuti_rule> &rule);

typedef std::function<bool (const std::string &, const std::string &, const kuti_morphw &)> checker_t;
inline void check_and_tag(const std::string &id, const std::vector<std::string> &ext_id, const std::string &match,
						  decltype(kuti_result::distn) &distn, const kuti_result::iterator &rt,
						  const std::vector<std::string> &words,
						  checker_t checker)
{
	for (auto wt = words.cbegin(); wt != words.cend(); wt++) {
		if (checker(match, (*wt), (*rt))) {
			(*rt).category.push_back(id);
			distn.insert(std::make_pair(id, match[0]));
			(*rt).category.insert((*rt).category.end(), ext_id.begin(), ext_id.end());
			for (const auto &s : ext_id)
				distn.insert(std::make_pair(s, match[0]));
		}
	}
}

void proc_result(kuti_result &result)
{
	for (auto it = kuti_module::all().cbegin(); it != kuti_module::all().cend(); it++) {
		int c = 0;
		for (auto rt = result.begin(); rt != result.end(); rt++) {
			c++;
			checker_t checker;
			switch ((*it).second.kmatch[0]) {
			case 'N':
			case 'V':
			case 'Z':
				checker = [](const std::string &match, const std::string &word, const kuti_morphw &mw) {
					return match[0] == CAST_UC(*mw.phresult).pos &&
						!word.compare(CAST_UCSZ(*mw.phresult).stem) ||
						match[1] == '+' && std::string(CAST_UCSZ(*mw.phresult).stem).find(word) != string::npos;
				};
				break;
			case 'j':
				checker = [](const std::string &match, const std::string &word, const kuti_morphw &mw) {
					return !word.compare(CAST_UCSZ(*mw.phresult).josa);
				};
				break;
			case 'e':
				checker = [](const std::string &match, const std::string &word, const kuti_morphw &mw) {
					return !word.compare(CAST_UCSZ(*mw.phresult).eomi);
				};
				break;
			default:
				throw kuti_error(eBadSemantic, -1);
			}
			check_and_tag((*it).first, (*it).second.extends, (*it).second.kmatch, result.distn, rt, (*it).second.kwords, checker);
		}
	}

	for (auto it = kuti_rule::all().cbegin(); it != kuti_rule::all().cend(); it++) {
		int c = 0;
		for (auto rt = result.begin(); rt != result.end(); rt++) {
			c++;
			checker_t checker;
			switch ((*it).second.match[0]) {
			case 'N':
			case 'V':
			case 'Z':
				checker = [](const std::string &match, const std::string &word, const kuti_morphw &mw) {
					return match[0] == CAST_UC(*mw.phresult).pos &&
						!word.compare(CAST_UCSZ(*mw.phresult).stem) ||
						match[1] == '+' && std::string(CAST_UCSZ(*mw.phresult).stem).find(word) != string::npos;
				};
				break;
			case 'j':
				checker = [](const std::string &match, const std::string &word, const kuti_morphw &mw) {
					return !word.compare(CAST_UCSZ(*mw.phresult).josa);
				};
				break;
			case 'e':
				checker = [](const std::string &match, const std::string &word, const kuti_morphw &mw) {
					return !word.compare(CAST_UCSZ(*mw.phresult).eomi);
				};
				break;
			default:
				throw kuti_error(eBadSemantic, -2);
			}
			check_and_tag((*it).first, (*it).second.extends, (*it).second.match, result.distn, rt, (*it).second.words, checker);
		}
	}
	
	for (auto it = result.begin(); it != result.end(); it++) {
		result.packed.push_back(
			kuti_wordpack(std::string(CAST_UCSZ(*(*it).phresult).stem), CAST_UC(*(*it).phresult).pos, (*it).category));
	}

#if LOG & DEBUG
	printf(DBG_FMT_DELIM, "category tagged");
	for (auto rt = result.packed.begin(); rt != result.packed.end(); rt++) {
		std::cout << rt->stem << "[ ";
		for (auto ct = rt->category.begin(); ct != rt->category.end(); ct++) {
			std::cout << *ct << " ";
		}
		std::cout << "]" << std::endl;
	}
#endif

	apply_rule(result.packed, kuti_rule::all());

#if LOG & DEBUG
	printf(DBG_FMT_DELIM, "after rewrite and replace");
	for (auto rt = result.packed.begin(); rt != result.packed.end(); rt++) {
		std::cout << rt->stem << "[ ";
		for (auto ct = rt->category.begin(); ct != rt->category.end(); ct++) {
			std::cout << *ct << " ";
		}
		std::cout << "]" << std::endl;
	}
#endif
}
