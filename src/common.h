#pragma once

#include <cstdio>

extern "C" {
#include <ham-kma.h>
}

#define CAST_UC (char)(unsigned char) // HAM_UCHAR to (const) char if indeed
#define CAST_UCSZ (char *)(unsigned char *) // HAM_UCHAR [] to (const) char * if indeed
