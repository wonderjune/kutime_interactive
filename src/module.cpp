#include "module.h"
#include "debug.h"
#include "utils.h"
#include "types.h"
#include <cstring>
#include <iostream>

std::map<std::string, kuti_module> kuti_module::_all;

std::map<std::string, kuti_module> &kuti_module::all()
{
	return _all;
}

void kuti_module::load(std::string name, std::ifstream &fin, kb_fopen_t kb_fopen)
{
	kuti_module mod;
	std::map<std::string, std::vector<std::string> > content;
	char magic[1024];

	if (kuti_module::_all.find(name) != kuti_module::_all.end())
		return;

	parse_file(fin, magic, content, kb_fopen);

	if (strcmp(magic, "#(domain)")) {
		throw eBadModule;
	}

	if (content.find("ID") == content.end() || content["ID"].size() != 1) {
		throw eParseFailed;
	}
	
	mod.id = content["ID"][0];
	mod.desc = content["DESC"][0];
	mod.deplist = content["USING"];
	mod.kmatch = content["KMATCH"][0];
	mod.kwords = content["KWORDS"];
	if (content.find("VARS") != content.end()) {
		for (auto it = content["VARS"].begin(); it != content["VARS"].end(); it++) {
			std::string key, value;
			split(PARSE_DELIM, *it, key, value);
			mod.vars.push_back(make_pair(key, value));
		}
	}
	if (content.find("VNASSN") != content.end()) {
		for (auto it = content["VNASSN"].begin(); it != content["VNASSN"].end(); it++) {
			std::string key, value;
			split(PARSE_DELIM, *it, key, value);
			mod.vnassn[key] = value;
		}
	}
	if (content.find("RESBASE") != content.end() && content.find("RESSCHEMA") == content.end())
		throw eParseFailed;
	if (content.find("RESSCHEMA") != content.end()) {
		kuti_database db; // Assume a domain corresponds to an abstract database driver
		db.cols = content["RESSCHEMA"].size();
		for (int i = 0; i < db.cols; i++) {
			db.column[i] = content["RESSCHEMA"][i];
		}
		if (content.find("RESBASE") != content.end()) {
			for (auto it = content["RESBASE"].begin(); it != content["RESBASE"].end(); it++) {
				kuti_database::row row;
				int i = 0;
				per_split("\t", *it, [&i, &row](const std::string tok){ row.data[i] = tok; i++; });
				db.rows.push_back(row);
			}
		}
		mod.resbase = kuti_database::all().size();
		kuti_database::all().push_back(db);
	}
	if (content.find("REACT") != content.end()) {
		for (const auto &t : content["REACT"]) {
			if (!mod.react.empty())
				mod.react += "\n";
			mod.react += t;
		}
	}
	
	if (mod.id.compare(name)) {
		throw eParseFailed;
	}

#if LOG & VERBOSE
	std::cout << "[Module " << name << "]" << std::endl;
	dump(std::cout, content);
#endif
	
	content.erase("ID");
	content.erase("DESC");
	content.erase("USING");
	content.erase("KMATCH");
	content.erase("KWORDS");
	content.erase("VARS");
	content.erase("VNASSN");
	content.erase("RESBASE");
	content.erase("RESSCHEMA");
	content.erase("REACT");

	if (!content.empty()) {
		throw eBadModule;
	}

	for (auto it = mod.deplist.begin(); it != mod.deplist.end(); it++) {
		std::ifstream fin;
		kb_fopen(fin, (*it + ".r").c_str());
		kuti_rule::load(*it, fin, kb_fopen);
		fin.close();
	}

	kuti_module::_all[mod.id] = mod;
}

std::map<std::string, kuti_rule> kuti_rule::_all;

std::map<std::string, kuti_rule> &kuti_rule::all()
{
	return _all;
}

void kuti_rule::load(std::string name, std::ifstream &fin, kb_fopen_t kb_fopen)
{
	kuti_rule rd;
	std::map<std::string, std::vector<std::string> > content;
	char magic[1024];
	const char delim[] = PARSE_DELIM;

	if (kuti_rule::_all.find(name) != kuti_rule::_all.end())
		return;

	parse_file(fin, magic, content, kb_fopen);

	if (strcmp(magic, "#(category)")) {
		throw eBadModule;
	}
	
	if (content.find("ID") == content.end() || content["ID"].size() != 1) {
		throw eParseFailed;
	}

	rd.id = content["ID"][0];
	rd.desc = content["DESC"][0];
	if (content.find("DNINFER") != content.end())
		per_split(PARSE_DELIM, content["DNINFER"][0], [&rd](const std::string &tok){ rd.dninfer.push_back(tok); });
	if (content.find("EXTENDS") != content.end())
		rd.extends = content["EXTENDS"];
	if (content.find("MATCH") == content.end() || content["MATCH"].empty())
		throw eParseFailed;
	rd.match = content["MATCH"][0];
	rd.words = content["WORDS"];
	if (content.find("REWRITE") != content.end()) {
		for (auto it = content["REWRITE"].begin(); it != content["REWRITE"].end(); it++) {
			std::string key, value;
			split(PARSE_DELIM, *it, key, value);
			rd.rewrite.push_back(make_pair(key, value));
		}
	}
	if (content.find("REPLACE") != content.end()) {
		for (auto it = content["REPLACE"].begin(); it != content["REPLACE"].end(); it++) {
			std::string key, value;
			split(PARSE_DELIM, *it, key, value);
			rd.replace.push_back(make_pair(key, value));
		}
	}

	if (rd.id.compare(name)) {
		throw eParseFailed;
	}

#if LOG & VERBOSE
	std::cout << "[Rule " << name << "]" << std::endl;
	dump(std::cout, content);
#endif
	
	content.erase("ID");
	content.erase("DESC");
	content.erase("DNINFER");
	content.erase("EXTENDS");
	content.erase("MATCH");
	content.erase("WORDS");
	content.erase("REWRITE");
	content.erase("REPLACE");

	if (!content.empty()) {
		throw eParseFailed;
	}

	kuti_rule::_all[rd.id] = rd;
}

enum parse_mode {
	P_NORMAL,
	P_LIST,
	P_ECOUNT
};

void parse_file(std::ifstream &fin, buf_t &magic, content_t &content, kb_fopen_t kb_fopen)
{
	const char delim[] = PARSE_DELIM;

	int no = 0;
	char line[1024];

	parse_mode mode = P_NORMAL;
	std::string ab_parse_hint;

	fin.getline(magic, 1023);

	while (!fin.eof()) {
		no++;
		fin.getline(line, 1023);
		if (line[0] == '\0' || line[0] == '#') {
			continue;
		}

		if (mode == P_NORMAL) {
			std::string key, value;
			split(PARSE_DELIM, line, key, value);

			content[key] = values_t();

			if (!value.compare("/BEGIN LIST")) {
				mode = P_LIST;
				ab_parse_hint = key;
			} else if (value.length() >= 6 && !value.substr(0, 6).compare("/BEGIN")) {
				throw kuti_error(eParseFailed, no);
			} else if (value.length() >= 5 && !value.substr(0, 5).compare("/FILE")) {
				std::ifstream fin;
				kb_fopen(fin, value.substr(5 + strspn(strpbrk(value.c_str(), delim), delim)).c_str());
				parse_file(fin, content[key], kb_fopen);
				fin.close();
			} else {
				content[key].push_back(value);
			}
		} else {
			if (!strcmp(line, "/END"))
				mode = P_NORMAL;

			if (mode == P_LIST) {
				if (!strcmp(line, "/END LIST"))
					mode = P_NORMAL;
				else
					content[ab_parse_hint].push_back(line);
			}
		}
	}
}

void parse_file(std::ifstream &fin, values_t &value, kb_fopen_t kb_fopen)
{
	int no = 0;
	char line[1024];

	while (!fin.eof()) {
		no++;
		fin.getline(line, 1023);
		if (line[0] == '\0' || line[0] == '#') {
			continue;
		}

		value.push_back(line);
	}
}

void dump(std::ostream &out, content_t &content)
{
	for (auto it = content.begin(); it != content.end(); it++) {
		out << (*it).first << std::endl;
		int c = 0;
		for (auto jt = (*it).second.begin(); jt != (*it).second.end(); jt++) {
			if (++c > 10 && (*it).second.size() > 10) {
				out << "\t... and " << (*it).second.size() - 10 << " others" << std::endl;
				break;
			}

			out << "\t" << *jt << std::endl;
		}
	}
	out << "--------------------" << std::endl;
}

std::vector<kuti_database> kuti_database::_all;

std::vector<kuti_database> &kuti_database::all()
{
	return _all;
}
