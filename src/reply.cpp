#include "result.h"
#include "utils.h"
#include "types.h"

#include <vector>
#include <memory>
#include <functional>

class ReplyMacro {
protected:
	const std::string str;

public:
	ReplyMacro(const std::string &str) : str(str) {}
	virtual std::string resolve(kuti_synopsis &synopsis) const = 0;
};

class ReplyNonList : public ReplyMacro {
public:
	ReplyNonList(const std::string &str) : ReplyMacro(str) {}
};

class ReplyList : public ReplyMacro {
protected:
	std::vector<std::shared_ptr<ReplyNonList>> list;

public:
	ReplyList(const std::string &str);

	virtual std::string resolve(kuti_synopsis &synopsis) const {
		std::string ret;

		for (const auto &r : list) {
			ret += r->resolve(synopsis);
		}
		
		return ret;
	}
};

class RestrictedReply : public ReplyNonList {
protected:
	const ReplyList list;

public:
	RestrictedReply(const std::string &str) : ReplyNonList(str), list(str) {}
};

class IfReply : public RestrictedReply {
private:
	const std::string key;
	const std::string value;

public:
	IfReply(const std::string &key, const std::string &value, const std::string &str)
		: RestrictedReply(str)
		, key(key)
		, value(value)
	{
	}

	virtual std::string resolve(kuti_synopsis &synopsis) const
	{
		if (synopsis.vars[key] == value) {
			return list.resolve(synopsis);
		} else {
			return "";
		}
	}
};

class NonNilReply : public RestrictedReply {
private:
	const std::string key;

public:
	NonNilReply(const std::string &key, const std::string &str)
		: RestrictedReply(str)
		, key(key)
	{
	}

	virtual std::string resolve(kuti_synopsis &synopsis) const
	{
		if (synopsis.vars.find(key) != synopsis.vars.end()) {
			return list.resolve(synopsis);
		} else {
			return "";
		}
	}
};

class PrimitiveReply : public ReplyNonList {
public:
	PrimitiveReply(const std::string &str) : ReplyNonList(str) {}
};

class ConstantReply : public PrimitiveReply {
public:
	ConstantReply(const std::string &str) : PrimitiveReply(str)
	{
	}

	virtual std::string resolve(kuti_synopsis &synopsis) const
	{
		return str;
	}
};

class VariableReply : public PrimitiveReply {
private:
	const std::string key;

public:
	VariableReply(const std::string &key)
		: PrimitiveReply(key)
		, key(key)
	{
	}

	virtual std::string resolve(kuti_synopsis &synopsis) const
	{
		return synopsis.vars[key];
	}
};

ReplyList::ReplyList(const std::string &str)
	: ReplyMacro(str)
{
	std::function<void(const std::string &)> parse = [&](const std::string &str) -> void
	{
		if (str.empty())
			return;

		int offset(0);

		if (str[offset] == '{') {
			int count(1);

			for (++offset; count > 0 && offset < str.size(); ++offset) {
				if (str[offset] == '{') {
					++count;
				} else if (str[offset] == '}') {
					--count;
				}
			}

			if (count < 0 || count > 0 && offset == str.size())
				throw eBadModule;

			std::string token(str.begin() + 2, str.begin() + offset - 1);
			std::string first, first_rest, second, second_rest;
			split(PARSE_DELIM, token, first, first_rest);
			split(PARSE_DELIM, first_rest, second, second_rest);

			switch (str[1]) {
			case '=':
				list.push_back(std::make_shared<VariableReply>(token));
				break;
			case '!':
				list.push_back(std::make_shared<NonNilReply>(first, first_rest));
				break;
			case '?':
				list.push_back(std::make_shared<IfReply>(first, second, second_rest));
				break;
			default:
				throw eParseFailed;
			}

			parse(std::string(str.begin() + offset, str.end()));
		} else {
			while (offset < str.size() && str[offset] != '{') {
				++offset;
			}

			list.push_back(std::make_shared<ConstantReply>(std::string(str.begin(), str.begin() + offset)));

			parse(std::string(str.begin() + offset, str.end()));
		}
	};
	
	parse(str);
}

std::string subst(kuti_synopsis &synopsis, const std::string &react)
{
	return ReplyList(react).resolve(synopsis);
}
