#include "result.h"

#include <iostream>
#include <sstream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <memory>
#include <queue>
#include <set>

class RuleMatcher {
public:
	const std::string category;

	RuleMatcher(const std::string &category) : category(category) {}

	virtual bool match(kuti_wordpack &word) const = 0;
	virtual bool offset() const = 0;
};

class PosMatcher : public RuleMatcher {
private:
	const char pos;

public:
	PosMatcher(char pos)
		: RuleMatcher("")
		, pos(pos) {}

	bool match(kuti_wordpack &word) const
	{
		return (word.pos == pos);
	}

	bool offset() const
	{
		return true;
	}
};

class CatMatcher : public RuleMatcher {
public:
	CatMatcher(const std::string &category) : RuleMatcher(category) {}

	bool match(kuti_wordpack &word) const
	{
		for (auto it : word.category) {
			if (it == category) {
				return true;
			}
		}

		return false;
	}

	bool offset() const
	{
		return true;
	}
};

class MatcherContainer  {
private:
	std::vector<std::shared_ptr<RuleMatcher>> matchers;
	std::set<std::string> categories;

	bool irr;

public:
	typedef std::vector<std::shared_ptr<RuleMatcher>> matcher_type;

	MatcherContainer(const matcher_type & ms)
	{
		matchers.insert(matchers.end(), ms.begin(), ms.end());

		for (const auto &m : ms) {
			categories.insert(m->category);
		}

		irr = (categories.size() != 1);
	}

	const std::vector<std::shared_ptr<RuleMatcher>> &get() const
	{
		return matchers;
	}

	const bool is_irr() const
	{
		return irr;
	}

	void push_back(std::shared_ptr<RuleMatcher> matcher)
	{
		matchers.push_back(matcher);
		categories.insert(matcher->category);
	}

	bool contains(const std::string &cat) const
	{
		return (categories.find(cat) != categories.end());
	}
};

class ReplaceRule {
public:
	virtual std::vector<std::string> apply(std::vector<kuti_wordpack> &res) const = 0;
};

class OffsetRule : public ReplaceRule {
private:
	const int offset;

public:
	OffsetRule(const int offset) : offset(offset) {}

	std::vector<std::string> apply(std::vector<kuti_wordpack> &res) const
	{
		return std::vector<std::string>(1, res.at(offset).stem);
	}
};

class PlusRule : public ReplaceRule {
private:
	const std::shared_ptr<ReplaceRule> r1, r2;

public:
	PlusRule(const std::shared_ptr<ReplaceRule> r1, const std::shared_ptr<ReplaceRule> r2) : r1(r1), r2(r2) {}

	std::vector<std::string> apply(std::vector<kuti_wordpack> &res) const
	{
		auto rr1 = r1->apply(res);
		auto rr2 = r2->apply(res);

		if (rr1.size() > 1 || rr2.size() > 1)
			throw eBadModule;

		return std::vector<std::string>(1, std::to_string(atoi(rr1.at(0).c_str()) + atoi(rr2.at(0).c_str())));
	}
};

class SpaceRule : public ReplaceRule {
private:
	const std::shared_ptr<ReplaceRule> r1, r2;

public:
	SpaceRule(const std::shared_ptr<ReplaceRule> r1, const std::shared_ptr<ReplaceRule> r2) : r1(r1), r2(r2) {}

	std::vector<std::string> apply(std::vector<kuti_wordpack> &res) const
	{
		auto rr1 = r1->apply(res);
		auto rr2 = r2->apply(res);

		std::vector<std::string> ret;

		ret.insert(ret.end(), rr1.begin(), rr1.end());
		ret.insert(ret.end(), rr2.begin(), rr2.end());

		return ret;
	}
};


class ConcatRule : public ReplaceRule {
private:
	const std::shared_ptr<ReplaceRule> r1, r2;

public:
	ConcatRule(const std::shared_ptr<ReplaceRule> r1, const std::shared_ptr<ReplaceRule> r2) : r1(r1), r2(r2) {}

	std::vector<std::string> apply(std::vector<kuti_wordpack> &res) const
	{
		auto rr1 = r1->apply(res);
		auto rr2 = r2->apply(res);

		if (rr1.size() > 1 || rr2.size() > 1)
			throw eBadModule;

		return std::vector<std::string>(1, rr1[0] + rr2[0]);
	}
};

static MatcherContainer build_matcher(
	const std::string &category,
	const std::string &rule)
{
	MatcherContainer::matcher_type ret;

	for (auto it : rule) {
		if (it == '$') {
			ret.push_back(std::make_shared<CatMatcher>(category));
		} else {
			ret.push_back(std::make_shared<PosMatcher>(it));
		}
	}

	return ret;
}

static std::shared_ptr<std::vector<kuti_wordpack>> match(
	const std::vector<std::shared_ptr<RuleMatcher>> &matcher,
	std::vector<kuti_wordpack>::iterator it)
{
	std::shared_ptr<std::vector<kuti_wordpack>> ret;

	ret = std::make_shared<std::vector<kuti_wordpack>>();

	for (auto m : matcher) {
		if (!m->match(*it)) {
			return nullptr;
		}

		if (m->offset()) {
			ret->push_back(*it);
			++it;
			continue;
		}

		++it;
	}

	return ret;
}

static void word_rewrite(
	const CatMatcher &matcher,
	std::vector<kuti_wordpack> &words,
	const std::pair<std::string, std::string> &rewrite)
{
	for (auto &w : words) {
		size_t sz;

		if (!matcher.match(w))
			continue;
		
		while ((sz = w.stem.find(rewrite.first)) != std::string::npos) {
			w.stem.replace(sz, rewrite.first.size(), rewrite.second);
		}
	}
}

static void apply_rewrite(
	const std::string &category,
	std::vector<kuti_wordpack> &words,
	const std::vector<std::pair<std::string, std::string> > &rewrite)
{
	for (auto r : rewrite) {
		word_rewrite(category, words, r);
	}
}

static std::shared_ptr<ReplaceRule> new_rp(
	std::queue<std::string> &splits)
{
	std::string top = splits.front();
	splits.pop();

	if (isdigit(top[0])) {
		return std::make_shared<OffsetRule>(atoi(top.c_str()));
	} else if (top == "PLUS") {
		std::shared_ptr<ReplaceRule> r1, r2;

		r1 = new_rp(splits);
		r2 = new_rp(splits);

		return std::make_shared<PlusRule>(r1, r2);
	} else if (top == "SPACE") {
		std::shared_ptr<ReplaceRule> r1, r2;

		r1 = new_rp(splits);
		r2 = new_rp(splits);

		return std::make_shared<SpaceRule>(r1, r2);
	} else if (top == "CONCAT") {
		std::shared_ptr<ReplaceRule> r1, r2;

		r1 = new_rp(splits);
		r2 = new_rp(splits);

		return std::make_shared<ConcatRule>(r1, r2);
	} else {
		throw eBadModule;
	}
}

static std::shared_ptr<ReplaceRule> build_replace(
	const std::string &str)
{
	std::queue<std::string> splits;
	std::string tmp;

	std::stringstream ss(str);
	while (ss >> tmp) {
		splits.push(tmp);
	}

	if (splits.empty())
		return nullptr;
	
	return new_rp(splits);
}

static bool matcher_replace(
	const MatcherContainer &matchers,
	const std::string &rule,
	std::vector<kuti_wordpack> &words)
{
	const auto &matcher = matchers.get();

	if (words.size() < matcher.size())
		return false;

	for (auto i = 0; i <= words.size() - matcher.size(); ++i) {
		auto ret = match(matcher, words.begin() + i);

		if (ret) {
			std::vector<std::string> ncat;
			for (auto it = words.begin() + i; it != words.begin() + i + matcher.size(); it++) {
				for (const auto &cat : it->category) {
					if (!matchers.is_irr() || !matchers.contains(cat)) {
						ncat.push_back(cat);
					}
				}
				//ncat.insert(ncat.begin(), it->category.begin(), it->category.end());
			}

			words.erase(words.begin() + i, words.begin() + i + matcher.size());

			std::vector<kuti_wordpack> nwords;

			std::string nstem;
			char npos;
			auto rp = build_replace(rule);

			if (rp != nullptr) {
				npos = ret->at(0).pos;

				auto r = rp->apply(*ret);

				for (auto &p : r) {
					nstem = p;

					nwords.push_back(kuti_wordpack(nstem, npos, ncat));
				}
			
				words.insert(words.begin() + i, nwords.begin(), nwords.end());
			}

			return true;
		}
	}

	return false;
}

static void apply_replace(
	const std::string &category,
	std::vector<kuti_wordpack> &words,
	const std::vector<std::pair<std::string, std::string> > &replace)
{
	for (auto r : replace) {
		auto matchers = build_matcher(category, r.first);

		while (matcher_replace(matchers, r.second, words));
	}
}

void apply_rule(std::vector<kuti_wordpack> &words, std::map<std::string, kuti_rule> &rules)
{
	using namespace std;

	for (auto rule : rules) {
		apply_rewrite(rule.first, words, rule.second.rewrite);
		apply_replace(rule.first, words, rule.second.replace);
	}
}
