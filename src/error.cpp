#include "error.h"
#include "debug.h"

kuti_error::kuti_error() : label(eUndefined), data(0)
{
}

kuti_error::kuti_error(enum kuti_error_label l) : label(l), data(0)
{
}

kuti_error::kuti_error(enum kuti_error_label l, int d) : label(l), data(d)
{
}

kuti_error &kuti_error::operator=(const enum kuti_error_label &l)
{
	label = l;
	return *this;
}

bool kuti_error::operator==(kuti_error &e) const
{
	return label == e.label;
}

bool kuti_error::operator==(enum kuti_error_label &l) const
{
	return label == l;
}

void proc_error(kuti_error_label e)
{
	proc_error(kuti_error(e));
}

void proc_error(kuti_error e)
{
#if LOG & ERROR
	switch (e.label) {
		case eSuccess:
			puts("성공.");
			break;

		case eBadModule:
			puts("KB 파일 형식에 문제가 있습니다(bad module).");
			break;

		case eParseFailed:
			printf("KB 파일 내용에 문제가 있습니다(parse failed). line %d\n", e.data);
			break;

		case eBadSemantic:
			printf("잘못된 문장 입력입니다. line %d\n", e.data);
			break;

		case eVerbNotFound:
			puts("동사가 없습니다.");
			break;

		case eDebugOnly:
			puts("Debug-use only.");
			break;

		case eUndefined:
		default:
			puts("Unknown error.");
	}
#endif
}
