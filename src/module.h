#pragma once

#include "types.h"
#include <map>
#include <vector>
#include <string>

typedef char buf_t[1024];
typedef std::vector<std::string> values_t;
typedef std::map<std::string, values_t> content_t;

void parse_file(std::ifstream &fin, buf_t &magic, content_t &content, kb_fopen_t kb_fopen);
void parse_file(std::ifstream &fin, values_t &value, kb_fopen_t kb_fopen);
void dump(std::ostream &out, content_t &content);
