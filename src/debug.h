#pragma once

#include "common.h"

#define ASSERT (1 << 0)
#define ERROR (1 << 1)
#define DEBUG (1 << 2)
#define VERBOSE (1 << 3)

#define _LOG_ASSERT (0x00000000 | ASSERT)
#define _LOG_ERROR (_LOG_ASSERT | ERROR)
#define _LOG_DEBUG (_LOG_ERROR | DEBUG)
#define _LOG_VERBOSE (_LOG_DEBUG | VERBOSE)

#define LOG _LOG_VERBOSE

#define DBG_FMT_DELIM "-------------------- %s\n"

void dbg_put_presult(FILE *fp, HAM_PRESULT pr, HAM_PRUNMODE mode);
