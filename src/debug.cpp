#include "debug.h"

/** @deprecated */
#define __BOUND % 0x100

/**	@section alternative
 *	#define DBG_PUT_DELIM "\t"
 */
#define DBG_PUT_DELIM " + "

void dbg_put_presult(FILE *fp, HAM_PRESULT pr, HAM_PRUNMODE mode)
{
	// for reference
	// mode is unused
	// simplified

	// 1. 어절 분류 / word pattern (HAM_PTN_* : uchar enum)
	// asterisk indicates if tagging was successful.
	if (1)
	printf(": %hhu%s\t", pr->patn, pr->dinf ? "*" : "");

	// 2. 어간 및 유형, 사전정보, 점수 / stem (string), type (HAM_POS_* : uchar), dict-info (uchar enum) and score
	// *** dict-info is convertible to string if indeed
	if (1)
		printf("%s(%c)[%hhu:%hhu]", pr->stem, pr->pos, pr->dinf, pr->score);

	// 3. 체언 어미 / n-suffix (uchar enum) *** convertible to string if indeed
	if (pr->nsfx)
		printf("%sns%hhu(%c)", DBG_PUT_DELIM, pr->nsfx, HAM_POS_SFX_N);

	// 4. 용언 어미 / v-suffix (uchar enum) *** convertible to string if indeed
	if (pr->vsfx)
		if (pr->vsfx == 1 || pr->vsfx == 14) // predicative postfix 'wi'
			( (pr->jomi.josa == JOSA_VAR_preWi || pr->jomi.josa == JOSA_VAR_preWi2) &&
			printf("%s%s(%c)", DBG_PUT_DELIM, pr->josa, HAM_POS_JOSA) ||
			1) &&
			printf("%svs%hhu(%c)", DBG_PUT_DELIM, pr->vsfx, HAM_POS_COPULA);
		else
			printf("%svs%hhu(%c)", DBG_PUT_DELIM, pr->vsfx, HAM_POS_SFX_V);

	// 5. 보조 용언 / auxiliary verb (string) and type 
	if (pr->xverb[0])
		if (pr->patn == HAM_PTN_NVM) // short case
			printf("%s%s(%c)", DBG_PUT_DELIM, pr->xverb, HAM_POS_VJXV); // short
		else // long case: put postfix/conjugation info first then put auxverb
			printf("%sx%hhu(%c)", DBG_PUT_DELIM, pr->jomi.xomitype, HAM_POS_EOMI) &&
			printf("%s%s(%c)", DBG_PUT_DELIM, pr->xverb, HAM_POS_XVERB);
	
	// 6. 선어말어미 / prefinal conjugation; p-omi (uchar enum) *** convertible to string if indeed
	if (pr->pomi)
		printf("%sp%hhu(%c)", DBG_PUT_DELIM, pr->pomi, HAM_POS_PEOMI);

	// 7. 어말어미 / final conjugation; e-omi (string)
	if (pr->eomi[0])
		printf("%s%s(%c)", DBG_PUT_DELIM, pr->eomi, HAM_POS_EOMI);

	// 8. 조사 / postfix; josa (string)
	if (pr->josa[0])
		printf("%s%s(%c)", DBG_PUT_DELIM, pr->josa, HAM_POS_JOSA);

	// newline
	puts("");
}
