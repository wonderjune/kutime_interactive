/* Text DB transformer */

#include <cstdio>
#include <cstring>
#include <string>
#include <set>

#define DAYWEEK_CNT 7
#define TIMEDAY_CNT 12

const char *DAYWEEK_DEF[DAYWEEK_CNT] = {
	"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
};
const char *TIMEDAY_DEF[TIMEDAY_CNT] = {
	"0800",
	"0900", "1030", "1200", "1300", "1400", "1530",
	"1700", "1800", "1900", "2000", "2100",
};

int main()
{
	FILE *fpi = fopen("kutime.mdr", "r");
	FILE *fpo = fopen("kutime.new.mdr", "w");
	char buf[2048];

	if (fpi == NULL) {
		puts("NO FILE");
		return 0;
	}
	if (fpo == NULL) {
		puts("FAIL FILE");
		return 0;
	}

	while (!feof(fpi) && fscanf(fpi, "%[^\r\n] ", buf)) {
		char *timeblk = strrchr(buf, '\t') + 1;
		std::set<std::pair<int, int> > list;
		
		char *ublk = timeblk;
		while (ublk = strchr(ublk, '(')) {
			int _dayweek, _timeday;
			ublk++;
			sscanf(ublk, "%d", &_timeday);
			ublk = strchr(ublk, ',') + 1;
			sscanf(ublk, "%d", &_dayweek);
			_timeday = (_timeday - 1) / 2;
			_dayweek = _dayweek - 1;
			if (_timeday < 0 || _timeday >= TIMEDAY_CNT)
				continue;
			if (_dayweek < 0 || _dayweek >= DAYWEEK_CNT)
				continue;
			list.insert(std::make_pair(_dayweek, _timeday));
		}
		
		std::string s;
		for (auto v : list) {
			s += std::string(DAYWEEK_DEF[v.first]);
			s += std::string(TIMEDAY_DEF[v.second]);
		}
		sprintf(timeblk, "%s", s.c_str());
		fprintf(fpo, "%s\n", buf);
	}

	fclose(fpi);
	fclose(fpo);
	return 0;
}
